## Parse Limesurvey XML.

- No condition / loop in the xml

- html file contains question literal, instruction and condition
  - extract literal / instruction / condition informations and output to question_instruction directory
  - note about duplicated literals:
    - I used the first instruction in the question item table
    - Please check literal_instruction_condition.csv file

### Input:

- xml file
- optional: html file 

### Output:
  - db_input_dir dir: 
    - will put these files into archivist insert pipeline

  - question_instruction dir:
    - extracted from html file 
    - file contains: question literal, two sets of instructions, condition


Next step:
  - run [archivist_insert_pipeline](https://gitlab.com/closer-cohorts1/archivist_insert) with files from db_input directory

