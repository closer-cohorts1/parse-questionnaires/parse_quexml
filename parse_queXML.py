#!/bin/env python3

"""
    Python 3
    Parse que xml file, https://github.com/ACSPRI/queXML
"""

import re
import os
from lxml import etree
import xml.etree.ElementTree as ET
from collections import OrderedDict
from bs4 import BeautifulSoup

import pandas as pd
import json
import pprint

pp = pprint.PrettyPrinter(indent=4)

def int_to_roman(num):
    """
        Convert integer to roman numeral.
    """

    roman = OrderedDict()
    roman[1000] = "m"
    roman[900] = "cm"
    roman[500] = "d"
    roman[400] = "cd"
    roman[100] = "c"
    roman[90] = "xc"
    roman[50] = "l"
    roman[40] = "xl"
    roman[10] = "x"
    roman[9] = "ix"
    roman[5] = "v"
    roman[4] = "iv"
    roman[1] = "i"

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)
            yield roman[r] * x
            num -= (r * x)
            if num <= 0:
                break
    if num == 0:
        return "0"
    else:
        return "".join([a for a in roman_num(num)])


def parse_sectionInfo_element(sectionInfoElement):
    """
    input: sectionInfo element of ET xml
    output: parsed dictionary
    """
    sectionInfo_dict = {}
    sectionInfo_dict['position'] = sectionInfoElement.find('position').text
    sectionInfo_dict['text'] = sectionInfoElement.find('text').text
    sectionInfo_dict['administration'] = sectionInfoElement.find('administration').text

    return sectionInfo_dict


def parse_response_element(responseElements, questionText):
    """
    input: all response elements of a question
    output: parsed dictionary
    """
    response_dict = {}
    response_list = []

    contingentQuestion_dict = {}

    # only one responseElement
    if len(responseElements) == 1:
        responseElement = responseElements[0]
        response_dict['varName'] = responseElement.attrib['varName']
        free = responseElement.find('free') # response
        if not free is None:
            response_dict['response_type'] = 'response'

            response_dict['label'] = free.find('label').text
            response_dict['format'] = free.find('format').text
            response_dict['length'] = free.find('length').text

        else:
            response_dict['response_type'] = 'code_list'

            category_all = responseElement.findall('fixed/category')
            category_list = []

            for x, category in enumerate(category_all):
                category_dict = {}
                category_dict['Code_Order'] = x + 1
                if category.find('value').text == '-oth-':
                    category_dict['Code_Value'] = category.find('value').text
                    category_dict['Category'] = category.find('label').text
                else:
                    category_dict['Code_Value'] = category.find('value').text
                    category_dict['Category'] = category.find('label').text

                contingentQuestion = category.find('contingentQuestion')
                if not contingentQuestion is None:
                    contingentQuestion_dict['varName'] = contingentQuestion.attrib['varName']
                    contingentQuestion_dict['text'] = contingentQuestion.find('text').text
                    contingentQuestion_dict['length'] = contingentQuestion.find('length').text
                    contingentQuestion_dict['response_type'] = 'response'
                    contingentQuestion_dict['format'] = 'text'

                category_list.append(category_dict)
            response_dict['code_list'] =  category_list

    elif len(responseElements) == 2:
        check_r = []
        for responseElement in responseElements:
            response_dict['varName'] = responseElement.attrib['varName']
            free = responseElement.find('free') # response
            if not free is None:
                check_r.append('r')
        # multiple text response
        if set(check_r) == {'r'}:
            responseElement = responseElements[0]
            response_dict['varName'] = responseElement.attrib['varName']
            free = responseElement.find('free') # response
            if not free is None:
                response_dict['response_type'] = 'response'
                response_dict['label'] = free.find('label').text
                response_dict['format'] = free.find('format').text
                response_dict['length'] = free.find('length').text
            # quick fix, will make this better
            responseElement = responseElements[1]
            contingentQuestion_dict['varName'] = responseElement.attrib['varName']
            free = responseElement.find('free') # response
            if not free is None:
                contingentQuestion_dict['response_type'] = 'response'
                contingentQuestion_dict['label'] = free.find('label').text
                contingentQuestion_dict['format'] = free.find('format').text
                contingentQuestion_dict['length'] = free.find('length').text

        # more than one responseElement, for example: one codelist and one text response
        else:
            #print(len(responseElements))
            response_dict['response_type'] = 'code_list'
            category_list = []

            for x, response in enumerate(responseElements):
                category_dict = {}
                category_dict['Code_Order'] = x + 1
                category_dict['Code_Value'] = response.attrib['varName']
                category_dict['Category'] = response.find('fixed/category/label').text

                contingentQuestion = response.find('fixed/category/contingentQuestion')
                if not contingentQuestion is None:
                    contingentQuestion_dict['varName'] = contingentQuestion.attrib['varName']
                    contingentQuestion_dict['text'] = contingentQuestion.find('text').text
                    contingentQuestion_dict['length'] = contingentQuestion.find('length').text
                    contingentQuestion_dict['response_type'] = 'response'
                    contingentQuestion_dict['format'] = 'text'
                category_list.append(category_dict)
            response_dict['code_list'] =  category_list
            # this type of codelist has no name, using part of the question text as label
            response_dict['varName'] = questionText.replace(' ', '')[0:20]

    return [response_dict, contingentQuestion_dict]


def parse_question_element(questionElement):
    """
    input: question element of ET xml
    output: parsed dictionary
    """
    question_dict = {}

    # remove html from string
    if questionElement.find('text') is None:
        q_text = ''
    else:
        soup = BeautifulSoup(questionElement.find('text').text, features="lxml")
        q_text = re.sub('<[^<]+?>|\n|\r', '', soup.text)

    question_dict['text'] = re.sub('<[^<]+?|\n>', '', q_text)
    responseElements = questionElement.findall('response')
    [response_dict_1, response_dict_2] = parse_response_element(responseElements, question_dict['text'])

    question_dict['response'] = [response_dict_1, response_dict_2]

    directive_dict = {}
    directiveElement = questionElement.find('directive')
    if not directiveElement is None:
        directive_dict['position'] = directiveElement.find('position').text
        directive_dict['text'] = directiveElement.find('text').text
        directive_dict['administration'] = directiveElement.find('administration').text
    question_dict['directive'] = directive_dict

    if questionElement.find('subQuestion') is None:
        question_dict['question_type'] = 'question item'
        question_dict['sub_question_list'] = None
    else:
        # seperate question grid to individual question items
        question_dict['question_type'] = 'question grid'

        sub_question_all = questionElement.findall('subQuestion')
        sub_question_list = []

        for x, sub_question in enumerate(sub_question_all):
            sub_question_dict = {}
            sub_question_dict['sub_question_index'] = x
            sub_question_dict['varName'] = sub_question.attrib['varName']
            sub_question_dict['text'] = sub_question.find('text').text
            sub_question_list.append(sub_question_dict)

        question_dict['sub_question_list'] =  sub_question_list

    return question_dict


def get_tables(root):
    """
    input: root of ET xml
    output: archivist input tables
    """

    # questionnaire name contains space, use id instead
    questionnaire_name = 'survey_' + root.attrib['id']

    seq_cols = ['Label', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df_sequence_empty = pd.DataFrame(columns=seq_cols)

    df_row = pd.DataFrame({'Label': questionnaire_name,
                                      'Parent_Type': 'CcSequence',
                                      'Parent_Name': None,
                                      'Branch': None,
                                      'Position': 1},
                                      index=[0])

    df_sequence_top = pd.concat([df_sequence_empty, df_row], ignore_index=True)


    statement_cols = ['Label', 'Literal', 'Parent_Type', 'Parent_Name', 'Branch', 'Position']
    df_statement_top  = pd.DataFrame(columns = statement_cols)

    # TODO: will convert question grid to questsion item
    question_cols = ['Label', 'Literal', 'Instructions', 'Response', 'Parent_Type', 'Parent_Name', 'Branch', 'Position', 'min_responses', 'max_responses', 'rd_order', 'Interviewee']
    df_question_top  = pd.DataFrame(columns = question_cols)

    response_cols =  ['Label', 'Type' ,'Type2', 'Format', 'Min', 'Max']

    # loop each section
    sections  = root.findall('./section')
    seq_index = 0
    ques_index = 0
    stat_index = 0
    old_sequence = ''
    code_list = []
    response_list = []
    df_sequence = pd.DataFrame(columns=seq_cols)
    df_statement = pd.DataFrame(columns = statement_cols)
    df_question = pd.DataFrame(columns = question_cols)
    for section in sections:
        seq_index += 1
        print('section id: ' + section.attrib['id'])
        current_sequence = questionnaire_name
        for elem in section.findall('*'):
            print(elem.tag)
            if elem.tag == 'sectionInfo':
                sectionInfo_dict = parse_sectionInfo_element(elem)
                # sequence
                if sectionInfo_dict['position'] == 'title':
                    current_sequence = sectionInfo_dict['text']
                    # Append rows in Empty Dataframe by adding dictionaries
                    df_row = pd.DataFrame({'Label': current_sequence,
                                           'Parent_Type': 'CcSequence',
                                           'Parent_Name': questionnaire_name,
                                           'Branch': None,
                                           'Position': seq_index},
                                           index=[0])
                    df_sequence = pd.concat([df_sequence_top, df_row], ignore_index=True)

                # one section id corresponds to this sectionInfo
                #statement
                else:
                    stat_index += 1
                    # remove html from string
                    if not sectionInfo_dict['text'] is None:
                        s_soup = BeautifulSoup(sectionInfo_dict['text'], features="lxml") 
                        statement_literal = re.sub('<[^<]+?>|\n|\r', '', s_soup.text)

                        df_row = pd.DataFrame({'Label':  's_' + str(stat_index),
                                               'Literal': statement_literal,
                                               'Parent_Type': 'CcSequence',
                                               'Parent_Name': current_sequence,
                                               'Branch': 0,
                                               'Position': 1 },
                                               index=[0])
                        df_statement = pd.concat([df_statement_top, df_row], ignore_index=True)
                        df_statement_top = df_statement

            elif elem.tag == 'question':
                if old_sequence != current_sequence:
                    ques_index = 1
                ques_index = ques_index + 1
                question_dict = parse_question_element(elem)
                # print(question_dict)

                # question item
                if question_dict['question_type'] == 'question item':
                    """
                    literal_text = question_dict['text']
                    if '</p>' in literal_text:
                        literal = literal_text.split('</p>')[0].replace('<p>', '').replace('<p>', '').strip()
                    else:
                        literal = literal_text
                    """
                    name_1 = ''
                    # print(question_dict['response'])
                    for idx, element in enumerate( question_dict['response'] ):

                        if element != {}:
                            if element['response_type'] == 'code_list':
                                df_code = pd.DataFrame(element['code_list'])
                                df_code.insert(loc=0, column='Label', value='cs_' + element['varName'])

                                code_list.append(df_code)
                                response = df_code['Label'][0]

                            elif element['response_type'] == 'response':
                                if element['format'] == 'date':
                                    response = 'Generic date'
                                    df_res = pd.DataFrame([[response, 'Date', 'Date', None, None, None]], columns = response_cols)

                                elif element['format'] == 'integer':
                                    #print(element)
                                    max_num = element['length']
                                    if element['label'] is not None:
                                        response = element['label']
                                    else:
                                        response = 'How many'
                                    df_res = pd.DataFrame([[response, 'Numeric', 'Integer', None, 0, None]], columns = response_cols)

                                elif element['format'] in ('text', 'longtext'):
                                    max_num = element['length']
                                    response = 'Generic text'
                                    df_res = pd.DataFrame([[response, 'Text', None, None, None, 255]], columns = response_cols)
                                else:
                                    print(element)
                                response_list.append(df_res)
                            else:
                                print(pp.pprint(question_dict))

                            if idx == 0:
                                name_1 = element['varName']
                            label = 'qi_' + name_1

                            df_row = pd.DataFrame({'Label': label,
                                                   'Literal': question_dict['text'],
                                                   'Instructions': None,
                                                   'Response': response,
                                                   'Parent_Type': 'CcSequence',
                                                   'Parent_Name': current_sequence,
                                                   'Branch': 0,
                                                   'Position': ques_index,
                                                   'min_responses': 1,
                                                   'max_responses': len([ d for d in question_dict['response'] if d]),
                                                   'rd_order': idx + 1,
                                                   'Interviewee': 'Cohort/sample member',
                                                   'Literal_original': question_dict['text'] },
                                                   index=[0])
                            df_question = pd.concat([df_question_top, df_row], ignore_index=True)

                            df_question_top = df_question

                # TODO: question grid, needs to convert each sub question to a question item
                elif question_dict['question_type'] == 'question grid':
                    for sub_index, subquestion in enumerate( question_dict['sub_question_list'] ):

                        label = subquestion['varName']
                        literal_original = question_dict['text']
                        if subquestion['text'] is not None:
                            literal = question_dict['text'] + ' ' + subquestion['text']
                        else:
                            literal = question_dict['text']

                        for idx, element in enumerate( question_dict['response'] ):

                            if element != {}:
                                if element['response_type'] == 'code_list':
                                    df_code = pd.DataFrame(element['code_list'])
                                    df_code.insert(loc=0, column='Label', value='cs_' + element['varName'])

                                    code_list.append(df_code)
                                    response = df_code['Label'][0]

                                elif element['response_type'] == 'response':
                                    if element['format'] == 'date':
                                        response = 'Generic date'
                                        df_res = pd.DataFrame([[response, 'Date', 'Date', None, None, None]], columns = response_cols)

                                    elif element['format'] == 'integer':
                                        max_num = element['length']
                                        if element['label'] is not None:
                                            response = element['label']
                                        else:
                                            response = 'How many'
                                        df_res = pd.DataFrame([[response, 'Numeric', 'Integer', None, 0, None]], columns = response_cols)

                                    elif element['format'] == 'text':
                                        max_num = element['length']
                                        response = 'Generic text '
                                        df_res = pd.DataFrame([[response, 'Text', None, None, None, 255]], columns = response_cols)

                                    response_list.append(df_res)
                                else:
                                    print('other response_type?')
                                    print(pp.pprint(question_dict))

                                df_row = pd.DataFrame({'Label': 'qi_' + label,
                                                       'Literal': literal,
                                                       'Instructions': None,
                                                       'Response': response,
                                                       'Parent_Type': 'CcSequence',
                                                       'Parent_Name': current_sequence,
                                                       'Branch': 0,
                                                       'Position': ques_index,
                                                       'min_responses': 1,
                                                       'max_responses': len([ d for d in question_dict['response'] if d]),
                                                       'rd_order': idx + 1,
                                                       'Interviewee': 'Cohort/sample member',
                                                       'Literal_original': literal_original },
                                                       index=[0])
                                df_question = pd.concat([df_question_top, df_row], ignore_index=True)

                                df_question_top = df_question
                        ques_index = ques_index + 1

                # this should not happen
                else:
                    print('CHECK: this should not happen')
                    print(pp.pprint(question_dict))

                old_sequence = current_sequence
            else:
                print('TODO: ' + elem.tag)

        df_sequence_top = df_sequence

    df_codelist = pd.concat(code_list)
    df_codelist.drop_duplicates(keep='first', inplace=True)
    df_response = pd.concat(response_list)
    df_response.drop_duplicates(keep='first', inplace=True)
    return df_sequence, df_statement, df_question, df_response, df_codelist


def get_instructions(htmlFile):
    """
    Get literal instruction pair from seperate html file
    """

    l = []
    d = {}
    with open(htmlFile) as fp: 
        soup = BeautifulSoup(fp, 'html.parser')

    q_text_all = soup.find_all('div', {'class' : 'q-text'}) 
    for item in q_text_all:
        literal = item.find('h3').text
        d['Literal'] = literal.replace('*', '').replace('\n', '').strip()

        instruction_all = item.find_all('div', {'class' : 'tip-help'})

        if instruction_all == []:
            instruction_1 = None
            instruction_2 = None
        elif len(instruction_all) == 1:
            ins = instruction_all[0]
            if len(ins.find_all('span', {'class' : 'fa'})) > 0:
                instruction_1 = ins.text.replace('\n', '').strip()
                instruction_2 = None
            else:
                instruction_1 = None
                instruction_2 = ins.text.replace('\n', '').strip()

        elif len(instruction_all) == 2:
            for ins in instruction_all:
                if len(ins.find_all('span', {'class' : 'fa'})) > 0:
                    instruction_1 = ins.text.replace('\n', '').strip()
                else:
                    instruction_2 = ins.text.replace('\n', '').strip()
        else:
            print('more than 2 instructions')
            print(instruction_all)

        d['Instructions_hidden'] = instruction_1
        d['Instructions'] = instruction_2
        condition = item.find('p', {'class' : 'q-scenaria'}).text.replace('\n', '').strip()
        d['htmlCondition'] = condition

        # d['htmlInstruction'] = [i.text.replace('\n', '').strip() for i in instruction_all]
        l.append(d)
        d = {}

    df_instruction = pd.DataFrame(l)

    return df_instruction


def main():
    main_dir = 'input'
    input_name = [f for f in os.listdir(main_dir) if f.endswith('.xml')]
    xmlFile = os.path.join(main_dir, input_name[0])
    print(xmlFile)

    output_dir = 'db_input_dir'
    #print(output_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    instruction_dir = 'question_instruction'
    if not os.path.exists(instruction_dir):
        os.makedirs(instruction_dir)

    tree = ET.parse(xmlFile, parser=ET.XMLParser(encoding="utf-8"))
    root = tree.getroot()

    df_sequence, df_statement, df_question, df_response, df_codelist = get_tables(root)

    # add question instruction from html
    input_html_name = [f for f in os.listdir(main_dir) if f.endswith('.html')]
    if input_html_name != []:
        htmlFile = os.path.join(main_dir, input_html_name[0])
        df_instruction = get_instructions(htmlFile)
        # output
        df_instruction.to_csv(os.path.join(instruction_dir, 'literal_instruction_condition.csv'), index=True, sep='\t')
        # there are duplicated literals, use the first one for now
        df_instruction.drop_duplicates(subset = ['Literal'], keep='first', inplace =True)
        # fill question instruction
        df_question['Instructions'] = df_question['Literal_original'].map(df_instruction.set_index('Literal')['Instructions'])
        # drop column
        df_question.drop(columns=['Literal_original'], inplace=True)

    # check if response contains both "How many I" and "How many F"
    # If there is only one "How many" can we label it to "How many" instead of "How many I"
    df_r = df_response[df_response['Label'].isin(['How many I', 'How many F'])]
    in_response = df_r['Label'].unique().tolist()
    if len(in_response) == 1:
        d_response_mod = {}
        d_response_mod[in_response[0]] = "How many"
        # modify question and response tables
        df_response['Label'].replace(d_response_mod, inplace=True)
        df_question['Response'].replace(d_response_mod, inplace=True)

    df_sequence.to_csv(os.path.join(output_dir, 'sequence.csv'), index=False, sep='\t')
    df_statement.to_csv(os.path.join(output_dir, 'statement.csv'), index=False, sep='\t')
    df_question.to_csv(os.path.join(output_dir, 'question_item.csv'), index=False, sep='\t')
    df_response.to_csv(os.path.join(output_dir, 'response.csv'), index=False, sep='\t')
    df_codelist.to_csv(os.path.join(output_dir, 'codelist.csv'), index=False, sep='\t')

if __name__ == "__main__":
    main()
